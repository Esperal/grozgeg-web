# Grozgeg Web

G R O Z G E G    E X T E N D E D    U N I V E R S E 

## Required softwares

### Scout

Pour gérer la compilation automatique des feuilles de style SCSS installer Scout, sur la mecque c'est méga top : https://scout-app.io/

En gros maintenant on bourre tout le CSS dans des fichiers en .scss (on peut tout à fait mettre du CSS de base dans des fichier SCSS, c'est fait pour)

Pourquoi SCSS ? Parce que ça permet de gérer des variable CSS au lieu de recopier mille fois le même code couleur partout dans le CSS, ça permet aussi de mieux le formatter. 
Le rôle de Scout dans tout ça c'est juste d'automatiquement generer les fichier CSS au bon endroit dès qu'on CTRL+S un fichier SCSS.

- Paramètrage de Scout
    - Input Folder : <repo git>/grozgeg-web/scss
    - Output Folder : <repo git>/grozgeg-web/css

- Vidéo qui montre comman on fè : https://www.youtube.com/watch?v=6zA78zMsH9w