// Variables
var name = '';

// INTRO - Routine d'intro du bot
function botIntro() {
    botui.message.add({
        human: false,
        content: 'Salam mon rouya, je m\'appelle Monique Ben Moktar.'
    }).then(function () {
        askName();
    });
}

// INTRO - Demande du nom
function askName() {
    botui.message.add({
        delay: 2000,
        content: 'Et toi comment t\'appelles-tu ?'
    }).then(function () {
        return botui.action.text({
            delay: 1000,
            action: {
                size: 18,
                icon: 'user',
                placeholder: 'je m\'appelle...'
            }
        });
    }).then(function (res) {
        botui.message.bot({
            content: res.value + " ? T'es sûr que tu veux pas changer mdr ?"
        });
        name = res.value; // sauvegarde nom
        return botui.action.button({
            action: [{
                icon: 'check',
                text: 'Confirmer',
                value: 'confirm'
            }, {
                icon: 'pencil',
                text: 'Editer',
                value: 'edit'
            }]
        });
    }).then(function (res) {
        if (res.value == 'confirm') {
            introEnd();
        } else {
            askName();
        }
    });
}

// INTRO - Fin de l'intro
function introEnd() {
    botui.message.add({
        delay: 1000,
        content: 'Dacodac'
    }).then(function () {
        return botui.message.add({
            delay: 1500,
            content: 'On va être honnête ' + name + ' c\'est un prénom de narvalo ptdr'
        });
    }).then(function () {
        return botui.message.add({
            delay: 2000,
            content: 'Fdp ça te va mieux je trouve'
        });
    }).then(function () {
        mainRoute();
    });
}

// Routine principale du bot
function mainRoute() {
    botui.message.add({
        delay: 3000,
        content: 'Alors fdp, qu\'est-ce que tu veux ?'
    }).then(function () {
        return botui.action.button({
            delay: 2000,
            addMessage: false,
            action: [{
                text: 'Où suis-je ?',
                value: 'GROZGEG'
            },
            {
                text: 'Parler librement',
                value: 'liberte'
            },
            {
                text: 'Te connaître',
                value: 'connaissance'
            },
            {
                text: 'Tourner la tête héé héé 🎵',
                value: 'TOURNICOTON'
            }]
        });
    }).then(function (res) {
        botui.message.human({
            delay: 500,
            content: res.text
        });
        if (res.value == 'GROZGEG') {
            meetGrozgeg();
        }
        else if (res.value == 'liberte') {
            goLiberte();
        }
        else if (res.value == 'connaissance') {
            sexChat();
        }
        else {
            $("body").addClass("tournicoton");
        }
    });
}

// mainRoute > Où suis-je
function meetGrozgeg() {
    botui.message.add({
        delay: 1000,
        content: 'Tu ne connais pas le GROZGEG Club ?'
    }).then(function () {
        return botui.message.add({
            delay: 2000,
            content: 'Le GROZGEG Club est un conclave para-religieux réfractaire aux haricots verts. Ses membres sont issus d\'écoles de renoms comme par exemple l\'école primaire.'
        });
    }).then(function () {
        mainRoute();
    });
}

// mainRoute > Parler librement
function goLiberte() {
    botui.message.add({
        delay: 1000,
        content: 'Wesh t\'as cru c\'était la libre antenne ici ?'
    }).then(function () {
        return botui.message.add({
            delay: 2000,
            content: 'J\'vais péter dans les yeux de ta grand mère'
        });
    }).then(function () {
        return botui.message.add({
            delay: 3000,
            content: 'Fdp'
        });
    }).then(function () {
        mainRoute();
    });
}

// mainRoute > Te connaître
function sexChat() {
    botui.message.add({
        delay: 3000,
        content: 'Qu\'est ce que tu veux savoir ?'
    }).then(function () {
        return botui.action.button(
            {
                delay: 500,
                addMessage: false,
                action: [
                    {
                        text: 'Quel âge as-tu ?',
                        value: 'sexChatAge'
                    },
                    {
                        text: 'Tu as un copain ? Ou une copine peut être ?',
                        value: 'sexChatCopain'
                    },
                    {
                        text: 'Ils font quelle taille tes lolos ? ^^',
                        value: 'sexChatLolos'
                    },
                    {
                        text: 'Parle moi mal',
                        value: 'sexChatMal'
                    }]
            });
    }).then(function (res) {
        botui.message.human
            (
                {
                    delay: 500,
                    content: res.text
                });
        if (res.value == 'sexChatAge') {
            sexChatAgeFct();
        }
        else if (res.value == 'sexChatCopain') {
            sexChatCopainFct();
        }
        else if (res.value == 'sexChatLolos') {
            sexChatLolosFct();
        }
        else if (res.value == 'sexChatMal') {
            sexChatMalFct();
        }
    });
}

// sexChat > Quel âge
function sexChatAgeFct() {
    botui.message.add({
        delay: 2000,
        content: 'Quel âge tu voudrais que j\'ai ?'
    }).then(function () {
        botui.action.select({
            action: {
                placeholder: 'Sélectionner l\'âge',
                value: '21',
                searchselect: false,
                label: 'text',
                options: [
                    { value: '12', text: '12 ans' },
                    { value: '21', text: '21 ans' },
                    { value: '34', text: '34 ans' },
                    { value: '57', text: '57 ans' },
                ],
                button: {
                    icon: 'check',
                    label: 'OK'
                }
            }
        }).then(function (res) {
            if (res.value == '12') {
                botui.message.add({
                    delay: 3000,
                    content: '12 ans ? J\'appelle les hendeks'
                }).then(function () {
                    mainRoute();
                });
            }

            else if (res.value == '21') {
                botui.message.add({
                    delay: 2000,
                    content: 'Ah ah t\es pas loin de la vérité'
                }).then(function () {
                    sexChat();
                });
            }
            else if (res.value == '34') {
                botui.message.add({
                    delay: 2000,
                    content: 'Tu les aimes avec un peu d\expérience mmh ?'
                }).then(function () {
                    sexChat();
                });
            }
            else if (res.value == '57') {
                botui.message.add({
                    delay: 2000,
                    content: 'Zarma t\es un gros pervers toi ! Dégage'
                }).then(function () {
                    mainRoute();
                });
            }
        });
    });
}

// sexChat > Copain
function sexChatCopainFct() {
    botui.message.add({
        delay: 2000,
        content: 'Non pas de copain. Par contre je connais une copine très très coquine, ça te dirait de participer à un plan à 3 ?'
    }).then(function () {
        return botui.action.button({
            delay: 500,
            addMessage: false,
            action: [{
                text: 'Oui',
                value: 'planTroisOui'
            },
            {
                text: 'Non',
                value: 'planTroisNon'
            },
            {
                text: 'Lama',
                value: 'planTroisLama'
            }]
        }).then(function (res) {
            botui.message.human({
                delay: 2000,
                content: res.text
            });
            if (res.value == 'planTroisOui') {
                botui.message.add({
                    delay: 2000,
                    content: 'Parfait amène ta caméra. Mon pote Diego va nous défoncer comme jaja'
                }).then(function () {
                    sexChat();
                });
            }
            else if (res.value == 'planTroisNon') {
                botui.message.add({
                    delay: 2000,
                    content: 'C\'était un test pour savoir si tu étais digne d\'Allah. Bravo à toi'
                }).then(function () {
                    mainRoute();
                });
            }
            else if (res.value == 'planTroisLama') {
                botui.message.add({
                    delay: 2000,
                    content: '🦙'
                }).then(function () {
                    sexChat();
                });
            }
        });
    });
}

// sexChat > Lolos
function sexChatLolosFct() {
	botui.message.add({
		delay: 2000,
		content: 'Mes lolos ? Seb c\'est toi ?'
	}).then(function(){
		return botui.message.add({
            delay: 4000,
            content: 'En vrai un beau 95D avec des gros mamelons très sensibles'
		});
	}).then(function(){
		sexChat();
	});
}

// sexChat > Parle mal
function sexChatMalFct() {
	botui.message.add({
		delay: 2000,
		content: 'Oh on est comme ça ? Je vais te parler mal. Très mal'
	}).then(function(){
		return botui.message.add({
			delay: 1500,
			content: 'écoute mwa bi1 ptit fdp ta reum jla goume'
		});
	}).then(function(){
		return botui.message.add({
			delay: 1500,
			content: 'ta famme jla prend jl\'encule'
		});
	}).then(function(){
		return botui.message.add({
			delay: 2000,
			content: 'tu doi te mouché ac du pq vu ta gueule 2 Q'
		});
	}).then(function(){
		return botui.message.add({
			delay: 2500,
			content: 'chui sur les meuf elles te dise pas bonjour tellement T moche. Elle change de trotoir'
		});
	}).then(function(){
		return botui.message.add({
			delay: 2500,
			content: 'boloss'
		});
	}).then(function(){
		sexChat();
	});
}